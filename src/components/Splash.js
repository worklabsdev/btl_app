import React, { Component } from "react";
import { AppRegistry, View, StyleSheet, Image, StatusBar } from "react-native";
export default class Splash extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      // style={style.container}
      <View>
        <StatusBar backgroundColor="#216061" barStyle="light-content" />
        <Image
          style={style.logo}
          source={require("../../images/intro/splash.png")}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#cc0000"
    // width: 450
  },
  logo: {
    height: "100%",
    width: "100%"
    // flex: 1
  }
});

AppRegistry.registerComponent("Splash", () => Splash);
