import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  KeyboardAvoidingView
} from "react-native";
import { Text, Icon, Button } from "native-base";
import { forgot_password } from "../actions";
import ValidationComponent from "react-native-form-validator";
import { connect } from "react-redux";
export default class Resetpassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      confirmPassword: "",
      otp: "",
      validateField: false
    };
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <View style={{ marginTop: 50, padding: 60 }}>
          <Text
            style={{
              textAlign: "center",
              fontSize: 25,
              marginBottom: 20,
              fontFamily: "sans-serif"
            }}
          >
            Reset Password
          </Text>

          <Text style={{ color: "silver", fontSize: 14, textAlign: "center" }}>
            {" "}
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,{" "}
          </Text>
        </View>

        {/* <Image style={styles.logo} source={require("../../images/logo.png")} /> */}
        <View style={styles.content}>
          <TextInput
            style={styles.textimp}
            placeholder="Enter New Password"
            placeholderTextColor="black"
            onChangeText={newPassword => this.setState({ newPassword })}
            value={this.state.newPassword}
          />
          {this.isFieldInError("newPassword") &&
            this.getErrorsInField("newPassword").map((errorMessage, index) => (
              <Text key={index}>{errorMessage}</Text>
            ))}
          <TextInput
            style={styles.textimp}
            placeholder="Confirm Password"
            placeholderTextColor="black"
            onChangeText={confirmPassword => this.setState({ confirmPassword })}
            value={this.state.confirmPassword}
          />
          {this.isFieldInError("confirmPassword") &&
            this.getErrorsInField("confirmPassword").map(
              (errorMessage, index) => <Text key={index}>{errorMessage}</Text>
            )}
          <TextInput
            style={styles.textimp}
            placeholder="Enter OTP"
            placeholderTextColor="black"
            onChangeText={otp => this.setState({ otp })}
            value={this.state.otp}
          />
          {this.isFieldInError("otp") &&
            this.getErrorsInField("otp").map((errorMessage, index) => (
              <Text key={index}>{errorMessage}</Text>
            ))}
          <TouchableOpacity
            style={styles.button}
            onPress={this.forgotpassword}

            block
          >
            <Text style={{ color: "#EF7E20", fontSize: 16 }}>Reset Password</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      alignItems: "center",
      flex: 1
      // flexDirection: "column"
    },
  
    content: {
      width: "85%",
      marginTop: -20
    },
    regForm: {
      backgroundColor: "#FF0000",
      flex: 1
    },
    logo: {
      height: 180,
      width: "45%",
      marginTop: 50
    },
    header: {
      fontSize: 24,
      color: "black",
      padding: 10,
      paddingBottom: 10,
      marginBottom: 40,
      borderBottomColor: "red",
      borderBottomWidth: 1
    },
    textimp: {
      color: "black",
      borderColor: "#efefef",
      backgroundColor: "#efefef",
      borderWidth: 1,
      textAlign: "center",
      borderRadius: 30,
      padding: 13,
      marginTop: 15
  
      // height: 20
    },
    btn: {
      marginTop: 20,
      padding: 7,
      color: "#FF0000"
    },
    btntext: {
      color: "red",
      fontWeight: "bold",
      fontSize: 15
    },
    button: {
      borderRadius: 25,
      alignItems: "center",
      // backgroundColor: "#EF7E20",
      borderWidth: 1,
      borderColor: "#EF7E20",
      padding: 13,
      marginTop: 30
    }
  });
  
  const mapStateToProps = state => {
    return {
      resetpasswordLoading: state.user.resetpasswordLoading,
      resetpasswordData: state.user.resetpasswordData
    };
  };
  
  export default connect(mapStateToProps)(Resetpassword);
  