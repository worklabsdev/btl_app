import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  KeyboardAvoidingView
} from "react-native";
import { Text, Icon, Button } from "native-base";
import { forgot_password } from "../actions";
import ValidationComponent from "react-native-form-validator";
import { connect } from "react-redux";
class Forgotpassword extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      validateField: false
    };
  }
  forgotpassword = () => {
    let vstatus = this.validate({
      email: { email: true, required: true }
    });
    this.setState({ validateField: vstatus });
    if (this.state.email && this.state.validateField) {
      let email = this.state.email;
      // alert("OTP has been sent on your registered email ID");
      // var { dispatch } = this.props;
      this.props.dispatch(forgot_password(email));
    }
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <View style={{ marginTop: 50, padding: 60 }}>
          <Text
            style={{
              textAlign: "center",
              fontSize: 25,
              marginBottom: 20,
              fontFamily: "sans-serif"
            }}
          >
            Forgot Password
          </Text>

          <Text style={{ color: "silver", fontSize: 14, textAlign: "center" }}>
            {" "}
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s,{" "}
          </Text>
        </View>

        {/* <Image style={styles.logo} source={require("../../images/logo.png")} /> */}
        <View style={styles.content}>
          <TextInput
            style={styles.textimp}
            placeholder="Enter Registered Email ID"
            placeholderTextColor="black"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />
          {this.isFieldInError("email") &&
            this.getErrorsInField("email").map((errorMessage, index) => (
              <Text key={index}>{errorMessage}</Text>
            ))}
          <TouchableOpacity
            style={styles.button}
            onPress={this.forgotpassword}
            // onPress={this.login}
            block
          >
            <Text style={{ color: "#EF7E20", fontSize: 16 }}>Submit</Text>
          </TouchableOpacity>
        </View>
        {/* <View style={{ marginTop: 20 }}>
          <View style={{ paddingBottom: 20, marginTop: 35 }}>
            <View style={{ justifyContent: "center" }}>
              <Text style={{ textAlign: "center", color: "darkgrey" }}>
                Login via
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 15,
                marginLeft: -3,
                marginBottom: 10,
                justifyContent: "center"
              }}
            >
              <Button rounded transparent>
                <Icon
                  style={{ fontSize: 35 }}
                  name="facebook-with-circle"
                  type="Entypo"
                />
              </Button>
              <Button rounded transparent style={{ boxShadow: 20 }}>
                <Image
                  source={require("../../images/ggg.jpg")}
                  style={{ height: 30, width: 30 }}
                />
              </Button>
            </View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text>Don't have and Account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Signupoptions")}
              >
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View> */}
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1
    // flexDirection: "column"
  },

  content: {
    width: "85%",
    marginTop: -20
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 180,
    width: "45%",
    marginTop: 50
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  textimp: {
    color: "black",
    borderColor: "#efefef",
    backgroundColor: "#efefef",
    borderWidth: 1,
    textAlign: "center",
    borderRadius: 30,
    padding: 13,
    marginTop: 15

    // height: 20
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    // backgroundColor: "#EF7E20",
    borderWidth: 1,
    borderColor: "#EF7E20",
    padding: 13,
    marginTop: 30
  }
});

const mapStateToProps = state => {
  return {
    mailsentLoading: state.user.mailsentLoading,
    forgotmailData: state.user.forgotmailData
  };
};

export default connect(mapStateToProps)(Forgotpassword);
