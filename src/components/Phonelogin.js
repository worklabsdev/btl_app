import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { login_phonenumber, change_number } from "../actions";
import { Text, Icon, Button, Toast } from "native-base";
import ValidationComponent from "react-native-form-validator";
import Facebook from "./Facebook";
class Phonelogin extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: ""
    };
  }

  login_phonenumber = () => {
    var vstatus = this.validate({
      phonenumber: {
        minlength: 10,
        required: true
      }
    });
    if (this.state.phonenumber && this.state.phonenumber.length == 10) {
      var { dispatch } = this.props;
      dispatch(login_phonenumber(this.state.phonenumber));
    } else {
      alert("Enter Valid Number");
    }
  };

  componentDidUpdate(prevProps, prevState) {
    // console.log("prevState", prevProps);
    // console.log("CURRENT STATE", this.props);
    // if (prevProps.loginphoneSuccess != this.props.loginphoneSuccess) {
    if (
      this.props.loginphoneSuccess == true &&
      this.props.phoneregisterData &&
      this.props.phoneregisterData.success == 1 &&
      this.props.loginphoneSuccessCheck == false
    ) {
      this.props.navigation.navigate("Otp");
      console.log("COMPONENT DID UPDATE", this.props.phoneregisterData.success);
    }
    // else if (this.props.loginphoneinvalid == true) {
    //   alert("Number not registetred! Please Signup");
    // }
    // }
  }
  // else if (
  //   this.props.loginphoneSuccess == false &&
  //   this.props.loginphonePresent == true
  // ) {
  //   // this.setState({ phonenumber: "" });
  //   Toast.show({
  //     text: "Phone Number already in use! Please Login",
  //     buttonText: "Okay",
  //     buttonTextStyle: { color: "#EF7E20" },
  //     buttonStyle: { backgroundColor: "#FFFFFF" },
  //     textStyle: { color: "#FFFFFF" },
  //     style: { backgroundColor: "#EF7E20" },
  //     duration: 3000
  //   });
  //   setTimeout(() => {
  //     this.props.navigation.navigate("Phonelogin");
  //   }, 2000);

  // console.log("COMPONENT DID UPDATE", this.props.phoneloginData);

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <Image style={styles.logo} source={require("../../images/logo.png")} />
        <View style={styles.content}>
          <TextInput
            style={styles.textimp}
            placeholder="Phone Number"
            placeholderTextColor="black"
            keyboardType="numeric"
            maxLength={10}
            onChangeText={phonenumber => this.setState({ phonenumber })}
            value={this.state.phonenumber}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={this.login_phonenumber}
            // disabled={!this.state.phonenumber.length < 10}
            // onPress={() => this.props.navigation.navigate("Otp")}
            block
          >
            <Text style={{ color: "#EF7E20", fontSize: 16 }}>Log In</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={{ paddingBottom: 20, marginTop: 25 }}>
            <View style={{ justifyContent: "center" }}>
              <Text style={{ textAlign: "center" }}>Login via</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 15,
                marginLeft: -3,
                marginBottom: 10,
                justifyContent: "center"
              }}
            >
              <Facebook />
              {/* <Button rounded transparent>
                <Icon
                  style={{ fontSize: 35 }}
                  name="facebook-with-circle"
                  type="Entypo"
                />
              </Button> */}
              <Button rounded transparent style={{ boxShadow: 20 }}>
                <Image
                  source={require("../../images/ggg.jpg")}
                  style={{ height: 30, width: 30 }}
                />
                {/* <Icon style={{ fontSize: 35 }} name="google" type="AntDesign" /> */}
              </Button>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text>Don't have an Account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Signupoptions")}
              >
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </View>
            {/* <View>
              <Text>
                Don't have and Account ?{" "}
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  SIGN UP
                </Text>
              </Text>
            </View> */}
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column"
  },

  content: {
    width: "85%",
    marginTop: 20
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 200,
    width: 210,
    alignItems: "center",
    marginTop: 25,
    marginBottom: 10
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  textimp: {
    color: "black",
    borderColor: "#efefef",
    backgroundColor: "#efefef",
    borderWidth: 1,
    textAlign: "center",
    borderRadius: 25,
    padding: 13,
    marginTop: 15

    // height: 20
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    // backgroundColor: "#EF7E20",
    borderWidth: 1,
    borderColor: "#EF7E20",
    padding: 13,
    marginTop: 30
  }
});

const mapStateToProps = state => {
  return {
    loginphoneLoading: state.user.loginphoneLoading,
    loginphoneSuccess: state.user.loginphoneSuccess,
    loginphoneSuccessCheck: state.user.loginphoneSuccessCheck,
    loginphoneinvalid: state.user.loginphoneinvalid,
    phoneregisterData: state.user.phoneregisterData
  };
};

export default connect(mapStateToProps)(Phonelogin);
