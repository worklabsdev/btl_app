import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import {
  Container,
  Header,
  Content,
  Thumbnail,
  Text,
  Toast,
  Icon,
  Button
} from "native-base";
import { register_user } from "../actions";
import { connect } from "react-redux";
import ValidationComponent from "react-native-form-validator";
import Facebook from "./Facebook";
class Emailsignup extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  register = () => {
    var vstatus = this.validate({
      email: { email: true, password: true },
      password: { required: true }
    });
    if (this.state.email && this.state.password && vstatus == true) {
      var userJson = {
        email: this.state.email ? this.state.email : "",
        password: this.state.password ? this.state.password : ""
      };
      var { dispatch } = this.props;
      dispatch(register_user(userJson));
    } else {
      // Toast.show({
      //   text: "Required Field's !",
      //   buttonText: "Okay",
      //   buttonTextStyle: { color: "#FF0000" },
      //   buttonStyle: { backgroundColor: "#FFF" },
      //   textStyle: { color: "white" },
      //   style: { backgroundColor: "red" },
      //   duration: 3000
      // });
    }
  };
  componentDidUpdate() {
    if (
      this.props.registerSuccess == true
      // this.props.regData.status == 200
    ) {
      Toast.show({
        text: "Successfully registered ! Please Login",
        textStyle: { color: "white" },
        style: { backgroundColor: "#0E8486" },
        duration: 3000
      });
      setTimeout(() => {
        this.props.navigation.navigate("Emaillogin");
      }, 2000);
    }
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <Image style={styles.logo} source={require("../../images/logo.png")} />
        <View style={styles.content}>
          <TextInput
            style={styles.textimp}
            placeholder="Email ID"
            placeholderTextColor="black"
            autoCapitalize="none"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />
          {this.isFieldInError("email") &&
            this.getErrorsInField("email").map((errorMessage, index) => (
              <Text key="index">{errorMessage}</Text>
            ))}
          <TextInput
            style={styles.textimp}
            placeholder="Password"
            placeholderTextColor="black"
            secureTextEntry={true}
            autoCapitalize="none"
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
          {this.isFieldInError("password") &&
            this.getErrorsInField("password").map((errorMessage, index) => (
              <Text key="index">{errorMessage}</Text>
            ))}
          <TouchableOpacity
            style={styles.button}
            onPress={this.register}
            disabled={this.props.regLoading}
            block
          >
            <Text style={{ color: "#EF7E20", fontSize: 16 }}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 20 }}>
          <View style={{ paddingBottom: 20, marginTop: 35 }}>
            <View style={{ justifyContent: "center" }}>
              <Text style={{ textAlign: "center" }}>Signup via</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 20,
                marginLeft: -3,
                marginBottom: 14,
                justifyContent: "center"
              }}
            >
              <Facebook />
              {/* <Button rounded transparent>
                <Icon
                  style={{ fontSize: 35 }}
                  name="facebook-with-circle"
                  type="Entypo"
                />
              </Button> */}
              <Button rounded transparent style={{ boxShadow: 20 }}>
                <Image
                  source={require("../../images/ggg.jpg")}
                  style={{ height: 30, width: 30 }}
                />
              </Button>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text> Already have an account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Loginoptions")}
              >
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  LOG IN
                </Text>
              </TouchableOpacity>
            </View>
            {/* <View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Signupoptions")}
            >
          
                Already have an account ?{" "}
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  LOG IN
                </Text>
                </TouchableOpacity>
              </Text>
            </View> */}
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column"
  },

  content: {
    width: "85%",
    marginTop: 20
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 200,
    width: 210,
    alignItems: "center",
    marginTop: 25,
    marginBottom: 10
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  textimp: {
    color: "black",
    borderColor: "#efefef",
    backgroundColor: "#efefef",
    borderWidth: 1,
    textAlign: "center",
    borderRadius: 25,
    padding: 13,
    marginTop: 15

    // height: 20
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    // backgroundColor: "#EF7E20",
    borderWidth: 1,
    borderColor: "#EF7E20",
    padding: 13,
    marginTop: 30
  }
});

// const mapStateToProps = state => {
//   return {};

// };

const mapStateToProps = state => {
  return {
    regLoading: state.user.regLoading,
    registerSuccess: state.user.registerSuccess,
    regData: state.user.regData
  };
};
export default connect(mapStateToProps)(Emailsignup);
