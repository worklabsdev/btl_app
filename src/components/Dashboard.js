import React, { Component } from "react";
import { StatusBar } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Content,
  Card,
  CardItem,
  Text,
  Button
} from "native-base";
import { connect } from "react-redux";
import { change_number } from "../actions";
class Dashboard extends Component {
  constructor(props) {
    super(props);
  }
  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      if (value !== null) {
        // We have data!!
        console.log("ASYNCSTORAGE VALUES", value);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {
    this._retrieveData();
  }
  logout() {
    // AsyncStorage.clear();
    this.props.dispatch(change_number());
    this.props.navigation.navigate("Loginoptions");
  }
  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <Header style={{ backgroundColor: "#FFFFFF" }}>
          <Left />
          <Body>
            <Text>Bamboo Tree</Text>
          </Body>
          <Right>
            <Button transparent onPress={() => this.logout()}>
              <Text style={{ color: "#216061" }}>LOGOUT</Text>
            </Button>
          </Right>
        </Header>
        <Content>
          <Card>
            {/* <CardItem header>
              <Text>NativeBase</Text>
            </CardItem> */}
            <CardItem>
              <Body>
                <Text>Thank you for Logging in</Text>
              </Body>
            </CardItem>
            {/* <CardItem footer>
              <Text>GeekyAnts</Text>
            </CardItem> */}
          </Card>
        </Content>
      </Container>
    );
  }
}

const MapStateToProps = state => {
  return {
    loginData: state.user.loginData,
    phoneregisterData: state.user.phoneregisterData,
    regData: state.user.regData
  };
};
export default connect(MapStateToProps)(Dashboard);
