import React, { Component } from "react";
import { View, Image, StyleSheet, StatusBar, Text } from "react-native";
import { Button } from "native-base";
// import { withRouter } from "react-router-native";
import { withNavigation } from "react-navigation";
class Achievement extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.mainContent}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <Image
          style={styles.image}
          source={require("../../images/intro/walkthrough-4.png")}
        />
        <View>
          <Text style={styles.title}>Achievements</Text>
          <Text style={styles.text}>
            Strategize for challenges {"\n"} Win coins and medals {"\n"} Flaunt
            leaderboard fame
          </Text>
        </View>
        <Button
          onPress={() => this.props.navigation.navigate("Signupoptions")}
          warning
          style={{
            marginTop: 27,
            paddingTop: 3,
            paddingBottom: 3,
            paddingLeft: 50,
            paddingRight: 50,
            borderWidth: 1,
            borderRadius: 50,
            borderColor: "#EF7F3A",
            backgroundColor: "white",
            alignContent: "center",
            alignSelf: "center"
          }}
        >
          <Text style={{ color: "#EF7F3A", fontWeight: "bold", padding: 0 }}>
            Get Started
          </Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: "center"
    // justifyContent: "space-around"
  },
  image: {
    height: "70%",
    width: "100%",
    alignItems: "center"
    // alignSelf: "stretch"
  },
  text: {
    color: "black",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16,
    fontFamily: "Arial"
  },
  title: {
    fontSize: 19,
    fontWeight: "bold",
    color: "black",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16,
    marginTop: 30,
    fontFamily: "Arial"
  }
});

export default withNavigation(Achievement);
