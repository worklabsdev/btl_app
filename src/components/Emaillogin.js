import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  ScrollView,
  Modal,
  KeyboardAvoidingView,
  TouchableHighlight
} from "react-native";
import Facebook from "./Facebook";
import { AsyncStorage } from "@react-native-community/async-storage";
import {
  Container,
  Header,
  Content,
  Thumbnail,
  Text,
  Toast,
  Icon,
  Button
} from "native-base";
import { login_user } from "../actions";
import ValidationComponent from "react-native-form-validator";
import { connect } from "react-redux";
import { LoginManager } from "react-native-fbsdk";

// import { ScrollView } from "react-native-gesture-handler";

class Emaillogin extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      validateFields: false,
      modalVisible: false
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  async loginFacebook() {
    try {
      let result = await LoginManager.logInWithPermissions([
        "email",
        "public_profile"
      ]);
      if (result.isCancelled) {
        alert("Login Was Cancelled");
      } else {
        alert("Successfully Logged In");
        console.log("result FACEBOOK", result);
        // this.props.navigation.navigate("Dashboard");
        // alert("SuccessFully ")
        // alert(
        //   "Login was successful with permissions " +
        //     result.grantedPermissions.toString()
        // );
      }
    } catch (error) {
      alert("Login Failed with error:" + error);
    }
  }

  login = () => {
    var vstatus = this.validate({
      email: { email: true, required: true },
      password: { required: true }
    });
    this.setState({ validateFields: vstatus });
    console.log("STATUSSSSSSSSSSS", vstatus);
    if (this.state.email && this.state.password && vstatus == true) {
      this.setState({ validateFields: false });
      var email = this.state.email;
      var password = this.state.password;
      var userjson = {
        email,
        password
      };
      var { dispatch } = this.props;
      dispatch(login_user(userjson));
    } else {
      this.state.validateFields = true;
      // Toast.show({
      //   text: "email, password both required !",
      //   buttonText: "Okay",
      //   buttonTextStyle: { color: "#EF7E20" },
      //   buttonStyle: { backgroundColor: "#FFFFFF" },
      //   textStyle: { color: "#FFFFFF" },
      //   style: { backgroundColor: "#EF7E20" },
      //   duration: 3000
      // });
    }
  };
  componentDidUpdate() {
    // if (prevProps.loginData == "") {
    //   this.props.loginData = state.user.loginData;
    //   console.log("IN PREV PROP", this.props.loginData);
    // }
    if (this.props.loginData) {
      console.log("This is data------", this.props.loginData);
    }

    if (
      this.props.userLoggedIn &&
      this.props.userLoggedIn == true &&
      this.props.loginData &&
      this.props.loginData.success == 1 &&
      this.props.loginData.data.auth
    ) {
      var dd = this.props.loginData;

      // console.log("DDDDDDDDDDDDDDDDDD", dd.response.userData.interests);
      if (this.props.loginData.auth) {
        var localData = {
          isLoggedIn: true,
          name: dd.data.user.name,
          email: dd.data.user.email,
          authToken: dd.auth
        };
        console.log("THIS IS LOCALDATA", localData);
        this._loadData(localData);
      }
      // alert("Successfully logged in");
      this.props.navigation.navigate("Dashboard");
    }
    // if (
    //   this.props.userLoggedIn == false &&
    //   this.props.loginData &&
    //   this.props.loginData.success != 1
    // ) {
    //   Toast.show({
    //     text: "Invalid Credentials !",
    //     buttonText: "Okay",
    //     buttonTextStyle: { color: "#FFF" },
    //     buttonStyle: { backgroundColor: "#FF0000" },
    //     textStyle: { color: "red" },
    //     style: { backgroundColor: "white" },
    //     duration: 3000
    //   });
    //   console.log("INVALID CREDS");
    // }
  }
  _loadData = async data => {
    await AsyncStorage.setItem("userData", JSON.stringify(data))
      .then(() => {
        console.log(
          "it is saved successfully",
          AsyncStorage.getItem("userData")
        );
      })
      .catch(() => {
        console.log("there was an error saving the product");
      });
  };
  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        {/* <Modal
          animationType="slide"
          transparent={true}
          animationType="fade"
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View
            style={{
              marginTop: "50%",
              backgroundColor: "white",
              padding: 50,
              height: 300,
              width: "60%",
              textAlign: "center",
              justifyContent: "center",
              boxShadow: 10,
              borderWidth: 1,
              borderColor: "red"
            }}
          >
            <View>
              <Text>Hello World!</Text>

              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Text>Hide Modal</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal> */}
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        <Image style={styles.logo} source={require("../../images/logo.png")} />
        <View style={styles.content}>
          <TextInput
            style={styles.textimp}
            placeholder="Email ID"
            placeholderTextColor="black"
            autoCapitalize="none"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />
          {this.isFieldInError("email") &&
            this.getErrorsInField("email").map((errorMessage, index) => (
              <Text key={index}>{errorMessage}</Text>
            ))}
          <TextInput
            style={styles.textimp}
            placeholder="Password"
            placeholderTextColor="black"
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
          {this.isFieldInError("password") &&
            this.getErrorsInField("password").map((errorMessage, index) => (
              <Text key={index}>{errorMessage}</Text>
            ))}
          <TouchableOpacity
            style={styles.button}
            onPress={this.login}
            // disabled={this.state.validateFields}
            block
          >
            <Text style={{ color: "#EF7E20", fontSize: 16 }}>Log In</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 10 }}>
          <View style={{ paddingBottom: 20, marginTop: 5 }}>
            <View style={{ justifyContent: "center" }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Forgotpassword")}
              >
                <Text style={{ textAlign: "center", color: "darkgrey" }}>
                  Forgot Password
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ justifyContent: "center", marginTop: 10 }}>
              <Text style={{ textAlign: "center", color: "darkgrey" }}>
                Login via
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                marginTop: 15,
                marginLeft: -3,
                marginBottom: 10,
                justifyContent: "center"
              }}
            >
              <Facebook />
              {/* <Button rounded transparent onPress={this.loginFacebook}>
                <Icon
                  style={{ fontSize: 35 }}
                  name="facebook-with-circle"
                  type="Entypo"
                />
              </Button> */}
              <Button rounded transparent style={{ boxShadow: 20 }}>
                <Image
                  source={require("../../images/ggg.jpg")}
                  style={{ height: 30, width: 30 }}
                />
                {/* <Icon style={{ fontSize: 35 }} name="google" type="AntDesign" /> */}
              </Button>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text>Don't have an Account ? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Signupoptions")}
              >
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </View>
            {/* <View>
              <Text>
                Don't have and Account ?{" "}
                <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                  SIGN UP
                </Text>
              </Text>
            </View> */}
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column"
  },

  content: {
    width: "85%",
    marginTop: 20
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 200,
    width: 210,
    alignItems: "center",
    marginTop: 25,
    marginBottom: 10
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  textimp: {
    color: "black",
    borderColor: "#efefef",
    backgroundColor: "#efefef",
    borderWidth: 1,
    textAlign: "center",
    borderRadius: 25,
    padding: 13,
    marginTop: 15

    // height: 20
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    // backgroundColor: "#EF7E20",
    borderWidth: 1,
    borderColor: "#EF7E20",
    padding: 13,
    marginTop: 30
  }
});

// const mapStateToProps = state => {
//   return {};
// };

const mapStateToProps = state => {
  return {
    userloginLoading: state.user.userloginLoading,
    userLoggedIn: state.user.userLoggedIn,
    loginData: state.user.loginData,
    authToken: state.user.authToken
  };
};
export default connect(mapStateToProps)(Emaillogin);
