import React, { Component } from "react";
import { Image, StatusBar, View, Text } from "react-native";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Left,
  Right,
  Body,
  connectStyle
} from "native-base";
import { connect } from "react-redux";
import { verify_otp, change_number } from "../actions";
import OtpInputs from "react-native-otp-inputs";

class Otp extends Component {
  constructor(props) {
    super(props);
  }
  verifyOtp(code) {
    console.log("VERIFY CODE", code.length);
    console.log("AUTH TOKEN", this.props.phoneregisterData.data.auth);
    let token = this.props.phoneregisterData.data.auth
      ? this.props.phoneregisterData.data.auth
      : "";

    if (code.length == 4 && token) {
      var { dispatch } = this.props;
      dispatch(verify_otp(code, token));
    } else {
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log("NEXT PROPS", nextProps);
    // console.log("LATEST PROPS", this.props);
    if (
      this.props.verifyotpSuccess == true &&
      this.props.verifyotpData.success == 1
    ) {
      this.props.navigation.navigate("Dashboard");
      alert("OTP VERFIED SUCCESSFULLY");
    }
    //  else if (
    //   this.props.verifyotpSuccess == false &&
    //   this.props.verifyotpInvalid == true &&
    //   this.props.verifyotpData.success == 0
    // ) {
    //   alert("INVALID OTP");
    // }
  }
  change_number() {
    // var { dispatch } = this.props;
    // var loginphoneSuccess = false;
    this.props.dispatch(change_number());

    // setTimeout(() => {
    this.props.navigation.goBack();
    // }, 2000);
  }
  // boBack() {
  //   this.props.navigation.goBack(null);
  // }
  render() {
    return (
      <Container>
        <Header span style={{ height: 310, backgroundColor: "#368587" }}>
          <StatusBar backgroundColor="#216061" barStyle="light-content" />
          <Left>
            <Button transparent onPress={() => this.change_number()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body
            style={{
              alignItems: "center",
              justifyContent: "center",
              alignContent: "center"
            }}
          >
            <Image
              style={{
                height: 128,
                width: 120,
                marginLeft: "55%",
                marginTop: -200
              }}
              source={require("../../images/icons/finger-print.png")}
            />
          </Body>
          <Right />
        </Header>

        <View
          style={{
            marginTop: 5,
            paddingBottom: 80,
            paddingLeft: 70,
            paddingRight: 70,
            paddingTop: 30,
            alignContent: "center",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "silver", fontSize: 20, paddingBottom: 20 }}>
            {this.props.phoneregisterData.data
              ? this.props.phoneregisterData.data.user.phoneNumber
              : ""}
          </Text>
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 21,
              paddingBottom: 20,
              color: "black"
            }}
          >
            Verification Code
          </Text>

          <Text
            adjustsFontSizeToFit={true}
            numberOfLines={2}
            style={{
              paddingLeft: 5,
              paddingRight: 5,
              paddingBottom: 40,
              textAlign: "center",
              color: "black"
            }}
          >
            OTP has been sent to you on your {"\n"} mobile number. Please enter
            it below
          </Text>
          <OtpInputs
            handleChange={code => this.verifyOtp(code)}
            focusedBorderColor="white"
            inputStyles={{
              backgroundColor: "#368587",
              color: "white",
              width: 45
            }}
            numberOfInputs={4}
          />
          <Text style={{ color: "silver", fontSize: 15, marginTop: 40 }}>
            Didn't reveived OTP?
          </Text>
          <Button
            bordered
            rounded
            warning
            onPress={() => this.change_number()}
            style={{
              marginTop: 30,
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 37,
              paddingRight: 37,
              alignContent: "center",
              alignSelf: "center"
            }}
          >
            <Text style={{ color: "#EF7F3A", fontWeight: "bold" }}>
              Change Number
            </Text>
          </Button>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    verfiyotpLoading: state.user.verfiyotpLoading,
    verifyotpSuccess: state.user.verifyotpSuccess,
    verifyotpData: state.user.verifyotpData,
    verifyotpInvalid: state.user.verifyotpInvalid,
    phoneregisterData: state.user.phoneregisterData
  };
};

export default connect(mapStateToProps)(Otp);
