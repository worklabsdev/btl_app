import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
  KeyboardAvoidingView
} from "react-native";
import {
  Container,
  Header,
  Content,
  Thumbnail,
  Text,
  Toast,
  Icon,
  Button
} from "native-base";
import { withNavigation } from "react-navigation";
import { StackNavigator } from "react-navigation";
import Facebook from "./Facebook";

class Loginoptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  navigatetoLogin() {
    const { navigate } = this.props.navigation;
    navigate("Emaillogin");
  }
  render() {
    // const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
        {/* <StatusBar barStyle="light-content" /> */}
        {/* <View
          style={{
            width: "100%",
            height: 400,
            textAlign: "center",
            alignItems: "center",
            marginTop: "5%"
          }}
        > */}
        <Image style={styles.logo} source={require("../../images/logo.png")} />
        {/* </View> */}
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate("Emaillogin")}
            block
            light
          >
            <Text style={{ color: "white", fontSize: 15 }}>
              Login by Email Id
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.props.navigation.navigate("Phonelogin");
            }}
            // disabled={this.props.userloginLoading}
            block
            light
          >
            <Text style={{ color: "white", fontSize: 15 }}>
              Login by Phone Number
            </Text>
          </TouchableOpacity>
        </View>

        <View style={{ paddingBottom: 20, marginTop: 40 }}>
          <View style={{ justifyContent: "center" }}>
            <Text style={{ textAlign: "center" }}>Login via</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              marginTop: 20,
              marginLeft: -3,
              marginBottom: 14,
              justifyContent: "center"
            }}
          >
            <Facebook />
            {/* <Button rounded transparent>
              <Icon
                style={{ fontSize: 35 }}
                name="facebook-with-circle"
                type="Entypo"
              />
            </Button> */}
            <Button rounded transparent>
              <Image
                source={require("../../images/ggg.jpg")}
                style={{ height: 30, width: 30 }}
              />
              {/* <Icon style={{ fontSize: 35 }} name="google" type="AntDesign" /> */}
            </Button>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text>Don't have an Account ? </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Signupoptions")}
            >
              <Text style={{ color: "#0E8486", fontWeight: "bold" }}>
                SIGN UP
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    flexDirection: "column",
    marginTop: 40
  },

  content: {
    // flex: 1,
    width: "85%",
    marginTop: 4
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 200,
    width: 210,
    alignItems: "center",
    marginTop: 35,
    marginBottom: 10
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 30,
    alignItems: "center",
    backgroundColor: "#EF7E20",
    borderWidth: 1,
    borderColor: "#EF7E20",
    padding: 17,
    // height: 30,
    marginTop: 30
  }
});

// const mapStateToProps = state => {
//   return {};
// };
export default withNavigation(Loginoptions);
