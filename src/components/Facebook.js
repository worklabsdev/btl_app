import React, { Component } from "react";
import { View } from "react-native";
import { LoginManager } from "react-native-fbsdk";
import { Button, Icon } from "native-base";
export default class Facebook extends Component {
  constructor(props) {
    super(props);
  }
  async loginFacebook() {
    try {
      let result = await LoginManager.logInWithPermissions([
        "email",
        "public_profile"
      ]);
      if (result.isCancelled) {
        alert("Login Was Cancelled");
      } else {
        alert("Successfully Logged In");
        console.log("result FACEBOOK", result);
        // this.props.navigation.navigate("Dashboard");
        // alert("SuccessFully ")
        // alert(
        //   "Login was successful with permissions " +
        //     result.grantedPermissions.toString()
        // );
      }
    } catch (error) {
      alert("Login Failed with error:" + error);
    }
  }
  render() {
    return (
      <View>
        <Button rounded transparent onPress={this.loginFacebook}>
          <Icon
            style={{ fontSize: 35 }}
            name="facebook-with-circle"
            type="Entypo"
          />
        </Button>
      </View>
    );
  }
}
