import React, { Component } from "react";
import { Text, View, StyleSheet, StatusBar } from "react-native";
import { Dimensions } from "react-native";
import Image from "react-native-scalable-image";
import { Icon } from "native-base";
import LoginOptions from "./Loginoptions";
import Achievement from "./Achievement";
import { Button } from "native-base";
import AppIntroSlider from "react-native-app-intro-slider";

const slides = [
  {
    key: "somethun",
    // title: "Welcome to Bamboo Tree",
    // text:"The go to app for better fitness and healthier \n lifestyle \n #LetsBeActiveIndia",
    image: require("../../images/intro/walkthrough-1.png"),
    // ../../images/intro/walkthrough-1.jpg
    backgroundColor: "#59b2ab"
  },
  {
    key: "slide2",
    title: "Walk 'n Win",
    text:
      "Active participation in friends and admin \n challenges lets you grab coins and medals.",
    image: require("../../images/intro/walkthrough-2.png"),
    backgroundColor: "#febe29"
  },
  {
    key: "slide3",
    title: "Challenges",
    text:
      "Unique multi-type challenges \n Friend vs Friend and \n Admin Challenges",
    image: require("../../images/intro/walkthrough-3.png"),
    backgroundColor: "#22bcb5"
  }
  // {
  //   key: "slide4",
  //   title: "Achievements",
  //   text: "Keep yourself healthy with bamboo tree life and become a champion",
  //   image: require("../../images/intro/walkthrough-4.png"),
  //   backgroundColor: "#22bcb5"
  // }
];
export default class Intro extends Component {
  state = {
    showRealApp: false
  };
  constructor(props) {
    super(props);
  }
  _renderDoneButton = () => {
    return (
      <Text style={{ marginTop: 10 }}>
        Next{" "}
        <Icon
          style={{ fontSize: 9 }}
          name="arrow-right"
          type="SimpleLineIcons"
        />
      </Text>
    );
  };
  _renderNextButton = () => {
    return (
      <Text style={{ marginTop: 10 }}>
        Next{" "}
        <Icon
          style={{ fontSize: 9 }}
          name="arrow-right"
          type="SimpleLineIcons"
        />
      </Text>
    );
  };
  _renderPrevButton = () => {
    return (
      <Text style={{ marginTop: 10 }}>
        <Icon
          style={{ fontSize: 9 }}
          name="arrow-left"
          type="SimpleLineIcons"
        />{" "}
        {""}Previous
      </Text>
    );
  };

  _renderItem = item => {
    return (
      <View style={styles.mainContent}>
        <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />

        <Image
          style={styles.image}
          width={Dimensions.get("window").width}
          source={item.image}
        />

        <View>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.text}>{item.text}</Text>
        </View>
      </View>
    );
  };
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  };
  render() {
    if (this.state.showRealApp) {
      return <Achievement />;
    } else {
      return (
        <AppIntroSlider
          renderItem={this._renderItem}
          slides={slides}
          renderNextButton={this._renderNextButton}
          renderPrevButton={this._renderPrevButton}
          renderDoneButton={this._renderDoneButton}
          // showSkipButton={true}
          showPrevButton={true}
          activeDotStyle={{ backgroundColor: "#0E8486" }}
          buttonTextStyle={{ color: "darkgrey", fontSize: 15 }}
          onDone={this._onDone}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  mainContent: {
    flex: 1,
    alignItems: "center"
    // justifyContent: "space-around"
  },
  image: {
    // height: "100%",
    // width: "95%",
    alignItems: "center"
    // alignSelf: "stretch"
  },
  text: {
    color: "darkgrey",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16,
    fontFamily: "Arial"
  },
  title: {
    fontSize: 19,
    color: "black",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16,
    marginTop: 30,
    fontFamily: "Arial"
  }
});
