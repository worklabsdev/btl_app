import globalConst from "../helpers/global";
import DeviceInfo from "react-native-device-info";
import { NavigationActions } from "react-navigation";
export const REGISTER_USER_BEGIN = "REGISTER_USER_BEGIN";
export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGSITER_USER_PRESENT = "REGSITER_USER_PRESENT";
export const REGISTER_USER_ERROR = "REGISTER_USER_ERROR";
export const LOGIN_USER_BEGIN = "LOGIN_USER_BEGIN";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_INVALID = "LOGIN_USER_INVALID";
export const LOGIN_USER_ERROR = "LOGIN_USER_ERROR";
export const FORGOT_PASSWORD_BEGIN = "FORGOT_PASSWORD_BEGIN";
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const FORGOT_PASSWORD_ERROR = "FORGOT_PASSWORD_ERROR";
export const REGISTER_PHONENUMBER_BEGIN = "REGISTER_PHONENUMBER_BEGIN";
export const REGISTER_PHONENUMBER_SUCCESS = "REGISTER_PHONENUMBER_SUCCESS";
export const REGISTER_PHONENUMBER_ERROR = "REGISTER_PHONENUMBER_ERROR";
export const REGISTER_PHONENUMBER_PRESENT = "REGISTER_PHONENUMBER_PRESENT";

export const LOGIN_PHONENUMBER_BEGIN = "LOGIN_PHONENUMBER_BEGIN";
export const LOGIN_PHONENUMBER_SUCCESS = "LOGIN_PHONENUMBER_SUCCESS";
export const LOGIN_PHONENUMBER_INVALID = "LOGIN_PHONENUMBER_INVALID";
export const LOGIN_PHONENUMBER_ERROR = "LOGIN_PHONENUMBER_ERROR";

export const VERIFY_OTP_BEGIN = "VERIFY_OTP_BEGIN";
export const VERIFY_OTP_SUCCESS = "VERIFY_OTP_SUCCESS";
export const VERIFY_OTP_INVALID = "VARIFY_OTP_INVALID";
export const VERIFY_OTP_ERROR = "VERIFY_OTP_ERROR";
export const CHANGE_NUMBER_BEGIN = "CHANGE_NUMBER_BEGIN";
// ======================= Login Actions  =======================

export const login_begin = () => ({
  type: LOGIN_USER_BEGIN
});

export const login_success = data => ({
  type: LOGIN_USER_SUCCESS,
  data
});
export const login_invalid = data => ({
  type: LOGIN_USER_INVALID,
  data
});
export const login_error = error => ({
  type: LOGIN_USER_ERROR,
  error
});

export const login_user = data => {
  return dispatch => {
    dispatch(login_begin());
    // console.warn("this is the data", data);
    // globalConst.serverip +
    console.log(globalConst.serverip);
    var postData = {
      email: data.email,
      password: data.password,
      timezone: DeviceInfo.getTimezone()
    };
    fetch(`${globalConst.serverip}user/login`, {
      body: JSON.stringify(postData),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        console.log("dataaa" + JSON.parse(JSON.stringify(dat)).status);
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("htis is teh status", dd);
        if (dd.success == 1) {
          console.log("this is data return ", dd);
          dispatch(login_success(dd));
        } else if (dd.success != 1) {
          console.log("INVALID CREDS", dd);
          alert("Invalid Credentails..");
          // dispatch(login_invalid(dd));
        }
        // else {
        //   dispatch(invalid_login(dat));
        // }
        return dat;
      })
      .catch(err => {
        // if()
        console.log("rerorrrrrr", err);
        dispatch(login_error(err));
        console.log("error" + err);
      });
  };
};

//  ====================== Register Actions ========================

export const register_begin = () => ({
  type: REGISTER_USER_BEGIN
});
export const register_success = data => ({
  type: REGISTER_USER_SUCCESS,
  data
});
export const register_error = error => ({
  type: REGISTER_USER_ERROR,
  error
});
export const register_user_present = () => ({
  type: REGSITER_USER_PRESENT
});

export const register_user = data => {
  return dispatch => {
    dispatch(register_begin());
    console.log("this is the data", data);
    var postJson = {
      email: data.email,
      password: data.password,
      timezone: DeviceInfo.getTimezone()
    };
    fetch(`${globalConst.serverip}user/signup`, {
      body: JSON.stringify(postJson),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("data got after saving", dd);
        if (dd.success == 1) {
          // console.log("this is data register ", dat);
          // this.props.navigation.navigate("Emaillogin");
          dispatch(register_success(dat));
        } else if (dd.success == 0) {
          alert(dd.message);
          dispatch(register_user_present());
        }

        // return dat;
      })
      .catch(err => {
        console.log("ERORRRRRRRRRR", err);
        dispatch(register_error(err));
      });
  };
};

// ==================== SIGN UP VIA PHONE NUMBER =================

export const register_phonenumber_begin = () => ({
  type: REGISTER_PHONENUMBER_BEGIN
});
export const register_phonenumber_success = data => ({
  type: REGISTER_PHONENUMBER_SUCCESS,
  data
});
export const register_phonenumber_error = error => ({
  type: REGISTER_PHONENUMBER_ERROR,
  error
});
export const register_phonenumber_present = () => ({
  type: REGISTER_PHONENUMBER_PRESENT
});

export const register_phonenumber = phonenumber => {
  console.log("ACTION", phonenumber);
  return dispatch => {
    dispatch(register_phonenumber_begin());
    console.log("this is phone number", phonenumber);
    var postJson = {
      phoneCode: "+91",
      phoneNumber: phonenumber,
      timezone: DeviceInfo.getTimezone()
    };
    fetch(`${globalConst.serverip}user/signup`, {
      body: JSON.stringify(postJson),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("SUCCESSFULLY REGISTERED NUMBER", dd);
        if (dd.success == 1) {
          console.log("SUCCESS number", dd.success);
          dispatch(register_phonenumber_success(dat));
        } else if (dd.success == 0) {
          alert("Phone Number already in use! Please Login");
          // dispatch(register_phonenumber_present(dat));
        }
      })
      .catch(err => {
        console.log("ERORRRRRRRRRR", err);
        dispatch(register_phonenumber_error(err));
      });
  };
};

// ========================== Login via phone number ========================

export const login_phonenumber_begin = () => ({
  type: LOGIN_PHONENUMBER_BEGIN
});
export const login_phonenumber_success = data => ({
  type: LOGIN_PHONENUMBER_SUCCESS,
  data
});
export const login_phonenumber_invalid = data => ({
  type: LOGIN_PHONENUMBER_INVALID,
  data
});
export const login_phonenumber_error = error => ({
  type: LOGIN_PHONENUMBER_ERROR,
  error
});

export const login_phonenumber = phonenumber => {
  console.log("ACTION", phonenumber);
  return dispatch => {
    dispatch(login_phonenumber_begin());
    console.log("this is phone number", phonenumber);
    var postJson = {
      phoneNumber: phonenumber,
      timezone: DeviceInfo.getTimezone()
    };
    fetch(`${globalConst.serverip}user/login`, {
      body: JSON.stringify(postJson),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("Number login successfull", dd);
        if (dd.success == 1) {
          console.log("SUCCESS number", dd.success);
          dispatch(login_phonenumber_success(dd));
          // this.props.navigation.navigate("Otp");
        } else if (dd.success == -1) {
          alert("Number not registetred! Please Signup");
          // console.log("Number not registetred! Please Signup");
          // dispatch(login_phonenumber_invalid(dd));
        }
      })
      .catch(err => {
        console.log("ERORRRRRRRRRR", err);
        dispatch(login_phonenumber_error(err));
      });
  };
};

// ================= verify OTP ======================

export const verify_otp_begin = () => ({
  type: VERIFY_OTP_BEGIN
});
export const verify_otp_success = data => ({
  type: VERIFY_OTP_SUCCESS,
  data
});
export const verify_otp_error = err => ({
  type: VERIFY_OTP_ERROR,
  err
});
export const verify_otp_invalid = data => ({
  type: VERIFY_OTP_INVALID,
  data
});

export const verify_otp = (otp, auth) => {
  console.log("THIS IS OTP", otp);
  return dispatch => {
    dispatch(verify_otp_begin());
    console.log("this is auth", auth);
    var postJson = {
      otp: otp
    };
    fetch(`${globalConst.serverip}user/verifyPhone`, {
      body: JSON.stringify(postJson),
      method: "post",
      headers: new Headers({
        Authorization: auth,
        "Content-Type": "application/json"
      })
    })
      .then(dat => dat.json())
      .then(dat => {
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("OTP verified", dd);
        if (dd.success == 1) {
          console.log("SUCCESS number", dd.success);
          dispatch(verify_otp_success(dd));
        } else if (dd.success == 0) {
          console.log("INVALID OTP", dd.success);
          alert("INVALID OTP");
          dispatch(verify_otp_invalid(dd));
        }
      })
      .catch(err => {
        console.log("ERORRRRRRRRRR", err);
        dispatch(verify_otp_error(err));
      });
  };
};

export const change_number_begin = () => ({
  type: CHANGE_NUMBER_BEGIN
});

export const change_number = loginphonesuccess => {
  return dispatch => {
    dispatch(change_number_begin());
  };
};
// ===============forgot password  =================================

export const forgotpassword_begin = () => ({
  type: FORGOT_PASSWORD_BEGIN
});
export const forgotpassword_success = data => ({
  type: FORGOT_PASSWORD_SUCCESS,
  data
});
export const forgotpassword_error = error => ({
  type: FORGOT_PASSWORD_ERROR,
  error
});
export const forgot_password = email => {
  console.log("ACTION", email);
  return dispatch => {
    dispatch(forgotpassword_begin());
    console.log("this is email", email);

    fetch(`${globalConst.serverip}users/passwordForgot`, {
      body: JSON.stringify(postJson),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(data => data.json())
      .then(data => {
        console.log("Getting data from this ", data.data);
        var dd = data.data;
        if (data.status == 200) {
          console.log("this is the status _______________", data);
          dispatch(forgotpassword_success(dd));
        }
      })
      .catch(error => {
        console.log("this is the error ", error);
        dispatch(forgotpassword_error(error));
      });
  };
};
