import {
  LOGIN_USER_BEGIN,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGIN_USER_INVALID,
  REGISTER_USER_BEGIN,
  REGISTER_USER_SUCCESS,
  REGSITER_USER_PRESENT,
  REGISTER_USER_ERROR,
  FORGOT_PASSWORD_BEGIN,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  REGISTER_PHONENUMBER_BEGIN,
  REGISTER_PHONENUMBER_SUCCESS,
  REGISTER_PHONENUMBER_ERROR,
  REGISTER_PHONENUMBER_PRESENT,
  LOGIN_PHONENUMBER_BEGIN,
  LOGIN_PHONENUMBER_SUCCESS,
  LOGIN_PHONENUMBER_INVALID,
  LOGIN_PHONENUMBER_ERROR,
  VERIFY_OTP_BEGIN,
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_INVALID,
  VERIFY_OTP_ERROR,
  CHANGE_NUMBER_BEGIN
} from "../actions";
const initialState = {
  userLoggedIn: false,
  userloginLoading: false,
  regLoading: false,
  registerSuccess: false,
  mailsentLoading: false,
  forgotmailData: {},
  loginData: {},
  regData: {},
  authToken: "",
  registerphoneLoading: false,
  registerphoneSuccess: false,
  registerphonePresent: false,
  phoneregisterData: {},
  verfiyotpLoading: false,
  verifyotpSuccess: false,
  verifyotpInvalid: false,
  verfiyotpData: {},
  loginphoneLoading: false,
  loginphoneSuccess: false,
  loginphoneinvalid: false,
  loginphoneSuccessCheck: false,
  phoneloginData: {}
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER_BEGIN:
      return {
        ...state,
        regLoading: true
      };
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        regLoading: false,
        registerSuccess: true,
        regData: action.data
      };
    case REGSITER_USER_PRESENT:
      return {
        ...state,
        regLoading: false,
        registerSuccess: false
      };
    case REGISTER_USER_ERROR:
      return {
        ...state,
        regLoading: false
      };
    case LOGIN_USER_BEGIN:
      return {
        ...state,
        userloginLoading: true,
        userLoggedIn: false
      };
    case LOGIN_USER_SUCCESS:
      console.log(
        "this is the user data++++++++++++++++++++++++",
        action.data.data.auth
      );
      // if(action.data.status==200){

      // }
      return {
        ...state,
        userLoggedIn: true,
        userloginLoading: false,
        loginData: action.data,
        authToken: action.data.data.auth
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        userLoggedIn: false,
        userloginLoading: false,
        loginData: action.data
      };
    case LOGIN_USER_INVALID:
      return {
        ...state,
        userLoggedIn: false,
        userloginLoading: false,
        loginData: action.data
      };
    case FORGOT_PASSWORD_BEGIN:
      return {
        ...state,
        mailsentLoading: true
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        mailsentLoading: false,
        forgotmailData: action.data
      };
    case FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        mailsentLoading: false
      };
    case REGISTER_PHONENUMBER_BEGIN:
      return {
        ...state,
        registerphoneLoading: true,
        registerphoneSuccess: false
      };
    case REGISTER_PHONENUMBER_SUCCESS:
      return {
        ...state,
        registerphoneLoading: false,
        registerphoneSuccess: true,
        phoneregisterData: action.data
      };
    case REGISTER_PHONENUMBER_ERROR:
      return {
        ...state,
        registerphoneLoading: false,
        registerphoneSuccess: false
      };
    case REGISTER_PHONENUMBER_PRESENT:
      return {
        ...state,
        registerphoneLoading: false,
        registerphoneSuccess: false,
        registerphonePresent: true,
        phoneregisterData: action.data
      };
    case VERIFY_OTP_BEGIN:
      return {
        ...state,
        verfiyotpLoading: true
      };
    case VERIFY_OTP_SUCCESS:
      return {
        ...state,
        verfiyotpLoading: false,
        verifyotpSuccess: true,
        verifyotpInvalid: false,
        verifyotpData: action.data
      };
    case VERIFY_OTP_INVALID:
      return {
        ...state,
        verfiyotpLoading: false,
        verifyotpSuccess: false,
        verifyotpInvalid: true,
        verifyotpData: action.data
      };

    case VERIFY_OTP_ERROR:
      return {
        ...state,
        verfiyotpLoading: false,
        verifyotpSuccess: false
      };

    case LOGIN_PHONENUMBER_BEGIN:
      return {
        ...state,
        loginphoneLoading: true,
        loginphoneSuccessCheck: true
      };
    case LOGIN_PHONENUMBER_SUCCESS:
      return {
        ...state,
        loginphoneLoading: false,
        loginphoneSuccess: true,
        loginphoneSuccessCheck: false,
        phoneregisterData: action.data
      };
    case LOGIN_PHONENUMBER_INVALID:
      return {
        ...state,
        loginphoneinvalid: true,
        loginphoneLoading: false,
        phoneregisterData: action.data,
        loginphoneSuccess: false
      };
    case LOGIN_PHONENUMBER_ERROR:
      return {
        ...state,
        loginphoneLoading: false
      };
    case CHANGE_NUMBER_BEGIN:
      return {
        ...state,
        verifyotpSuccess: false,
        verifyotpData: {},
        registerphoneSuccess: false,
        phoneregisterData: {},
        loginphoneSuccess: false,
        loginData: {},
        userLoggedIn: false,
        authToken: "",
        registerSuccess: false
      };
    default: {
      return {
        state
      };
    }
  }
};

export default user;
