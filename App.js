/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import store from "./src/store";
import { Provider } from "react-redux";
import { Root } from "native-base";
import Intro from "./src/components/Intro";
import Splash from "./src/components/Splash";
import Loginoptions from "./src/components/Loginoptions";
import Emaillogin from "./src/components/Emaillogin";
import Phonelogin from "./src/components/Phonelogin";
import Signupoptions from "./src/components/Signupoptions";
import Emailsignup from "./src/components/Emailsignup";
import Phonesignup from "./src/components/Phonesignup";
import Forgotpassword from "./src/components/Forgotpassword";
import Resetpassword from "./src/components/Resetpassword";
import Achievement from "./src/components/Achievement";
import Otp from "./src/components/Otp";
import Dashboard from "./src/components/Dashboard";
// import Signupoptions from "./src/components/Signupoptions";

import {
  createStackNavigator,
  createAppContainer,
  HeaderStyleInterpolation
} from "react-navigation";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

const navOption = {
  headerStyle: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: "#FFFFFF"
  },
  headerTintColor: "black"
};

const MainNavigator = createStackNavigator({
  Intro: {
    screen: Intro,
    navigationOptions: {
      header: null
    }
  },
  Loginoptions: {
    screen: Loginoptions,
    navigationOptions: navOption
  },
  Emaillogin: {
    screen: Emaillogin,
    navigationOptions: navOption
  },
  Phonelogin: {
    screen: Phonelogin,
    navigationOptions: navOption
  },
  Signupoptions: {
    screen: Signupoptions,
    navigationOptions: {
      header: null
    }
  },
  Emailsignup: {
    screen: Emailsignup,
    navigationOptions: navOption
  },
  Phonesignup: {
    screen: Phonesignup,
    navigationOptions: navOption
  },
  Forgotpassword: {
    screen: Forgotpassword,
    navigationOptions: navOption
  },
  Resetpassword: {
    screen: Resetpassword,
    navigationOptions: navOption
  },
  Otp: {
    screen: Otp,
    navigationOptions: {
      header: null
    }
  },
  Achievement: {
    screen: Achievement,
    navigationOptions: {
      header: null
    }
  },
  Dashboard: {
    screen: Dashboard,
    navigationOptions: {
      header: null
    }
  }
});
let Navigation = createAppContainer(MainNavigator);

// type Props = {};
{
  /* <Props> */
}
class App extends Component {
  state = {
    ready: false
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({ ready: true });
    }, 3000);
  }

  render() {
    return (
      <Provider store={store}>
        <Root>{this.state.ready ? <Navigation /> : <Splash />}</Root>
      </Provider>
      // <Navigation />
      //
      // <View style={styles.container}>
      //   <Text style={styles.welcome}>Welcome to React Native!</Text>
      //   <Text style={styles.instructions}>To get started, edit App.js</Text>
      //   <Text style={styles.instructions}>{instructions}</Text>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

export default App;
